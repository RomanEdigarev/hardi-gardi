import SlideArrowIcon from "./SlideArrowIcon";

export default {
  title: "UI/Icons/SlideArrowIcon",
  component: SlideArrowIcon,
};

const Template = (args) => ({
  components: {
    SlideArrowIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <SlideArrowIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
