import ArrowIcon from "./ArrowIcon";

export default {
  title: "UI/Icons/Arrow",
  component: ArrowIcon,
  parameters: {
    backgrounds: {
      default: "dark",
    },
  },
};

const Template = (args) => ({
  components: {
    ArrowIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <ArrowIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
