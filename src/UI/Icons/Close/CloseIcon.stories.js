import CloseIcon from "./CloseIcon";

export default {
  title: "UI/Icons/Close",
  component: CloseIcon,
};

const Template = (args) => ({
  components: {
    CloseIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <CloseIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
