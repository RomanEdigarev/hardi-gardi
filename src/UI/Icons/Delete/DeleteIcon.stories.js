import DeleteIcon from "./DeleteIcon";

export default {
  title: "UI/Icons/Delete",
  component: DeleteIcon,
};

const Template = (args) => ({
  components: {
    DeleteIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <DeleteIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
