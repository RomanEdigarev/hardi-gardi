import FavoriteIcon from "./FavoriteIcon";

export default {
  title: "UI/Icons/Favorite",
  component: FavoriteIcon,
};

const Template = (args) => ({
  components: {
    FavoriteIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <FavoriteIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
