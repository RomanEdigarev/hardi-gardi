import ShopIcon from "./ShopIcon";

export default {
  title: "UI/Icons/Shop",
  component: ShopIcon,
};

const Template = (args) => ({
  components: {
    ShopIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <ShopIcon v-bind="args"/>  
  `,
});

export const Icon = Template.bind({});
