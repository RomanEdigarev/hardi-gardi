import PlusIcon from "./PlusIcon";

export default {
  title: "UI/Icons/Plus",
  component: PlusIcon,
};

const Template = (args) => ({
  components: {
    PlusIcon,
  },
  setup() {
    return { args };
  },
  template: `
    <PlusIcon v-bind="args"/>  
  `,
});
